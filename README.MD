# Bitbucket Pipelines services tutorial

A sample Hello World application built with Node and MongoDB. This application is used in a Bitbucket Pipelines tutorial about integration testing.

## Requirements

  * Node and npm
  * MongoDB
